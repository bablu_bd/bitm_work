<?php
trait softDelete
{
    public function delete()
    {
        parent::delete();
        echo "Deleting from application but not from database<br/>";
    }
}

class Profile
{
    public function delete()
    {
        echo "Deleting this file<br/>";
    }

}
class Subclass extends Profile{
    use softDelete;
}

