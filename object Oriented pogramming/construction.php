<?php
class BaseClass{
    function  __construct()
    {
    print "In BaseClass constructor\n";
    }

}

class SubClass extends BaseClass{
    function  __construct()
    {
    parent ::__construct();
        print "In SubClass constructor\n";
    }
}

class OtherSubClass extends BaseClass{

}

$obj = new BaseClass();
echo "<br/>";
$obj1 = new SubClass();
echo "<br/>";
$obj2 = new OtherSubClass();
?>