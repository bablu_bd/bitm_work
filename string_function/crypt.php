<?php
if(CRYPT_STD_DES == 1)
{
    echo "standard DES:".crypt('something','st')."\n<br>";
}
else
{
    echo "standard DES not supported.\n<br>";
}

if(CRYPT_MD5 == 1)
{
    echo "MD5:".crypt('something','$1$somethin$')."\n<br>";
}
else
{
    echo "MD5 not supported.\n<br>";
}
if(CRYPT_BLOWFISH == 1)
{
    echo "Blowfish:".crypt('something ','$2a$09$anexamplestringforsalt$')."\n<br>";
}
else
{
    echo "Blowfish DES not supported.\n<br>";
}

if(CRYPT_SHA256 == 1)
{
    echo " SHA-256:".crypt('something','$5$rounds=5000$anexamplestringforsalt$')."\n<br>";
}
else
{
    echo "SHA-256 not supported.\n<br>";
}

if(CRYPT_SHA512 == 1)
{
    echo "SHA-512:".crypt('something','$6$rounds=5000$anexamplestringforsalt$');
}
else
{
    echo "SHA-512 not supported.";
}

?>