<?php
$input = "Alien";
echo str_pad($input,10);
echo "<br/>";
echo str_pad($input,10,"-=",STR_PAD_LEFT);
echo "<br/>";
echo str_pad($input,10,"-",STR_PAD_BOTH);
echo "<br/>";
echo str_pad($input,10,"__",STR_PAD_RIGHT);
echo "<br/>";
echo str_pad($input,3,"*");
echo "<br/>";
$str = "hello world";
echo str_pad($str,20,".");
echo "<br/>";
echo str_pad("input",10,"pp",STR_PAD_BOTH);
echo "<br/>";
echo str_pad("input",6,"p",STR_PAD_BOTH);
echo "<br/>";
echo str_pad("inptu",8,"p",STR_PAD_BOTH);
?>